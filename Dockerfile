FROM jenkins/jenkins

USER root

RUN apt-get update && \
    curl -fsSL get.docker.com -o get-docker.sh && \ 
    sh ./get-docker.sh && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER jenkins

COPY plugins.txt /usr/share/jenkins/ref/

RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt


